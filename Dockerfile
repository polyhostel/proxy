FROM node

RUN mkdir -p /usr/src/proxy/
WORKDIR /usr/src/proxy/

COPY . /usr/src/proxy/

#RUN npm install -g npm@7.0.5
#RUN npm i

EXPOSE 3001

CMD ["npm" , "run","start"]