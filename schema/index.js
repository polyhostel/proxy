const graphql = require('graphql')
const {
    UserType,
    StudentType,
    WashSessionType
} = require('./types')

const {
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLList,
    GraphQLString,
    GraphQLID,
} = graphql;

const {userRequests, hostelRequests, washRequests} = require('../api-requests')

const Query = new GraphQLObjectType({
    name: 'Query',
    fields: {
        // users
        allUsers: {
            type: new GraphQLList(UserType),
            resolve() {
                return userRequests.getAllUsers();
            },
        },

        //students
        getStudentByLp: {
            type: StudentType,
            args: {login: {type: GraphQLString}, password: {type: GraphQLString}},
            async resolve(parent, args) {
                const user_id = await userRequests.getUserByLP(args.login, args.password);
                const student = await userRequests.getStudentsByUser(user_id);
                student.room_in_hostel = await hostelRequests.getRoomInHostel(student.room_in_hostel_id)
                return student;
            },
        },

        getStudentByUserId: {
            type: StudentType,
            args: {userId: {type: GraphQLID,}},
            async resolve(parent, args) {
                const student = await userRequests.getStudentsByUser(args.userId)
                student.room_in_hostel = await hostelRequests.getRoomInHostel(student.room_in_hostel_id)
                return student
            }
        },

        studentsByRoomInHostelId: {
            type: new GraphQLList(StudentType),
            args: {roomInHostel: {type: GraphQLID,}},
            async resolve(parent, args) {
                let students
                students = await userRequests.getStudentsByRoomInHostelId(args.roomInHostel)
                await students.forEach((student) => {
                    student.room_in_hostel = hostelRequests.getRoomInHostel(student.room_in_hostel_id)
                })
                return students
            }
        },
        //    wash sessions
        allWashSessions: {
            type: new GraphQLList(WashSessionType),
            args: {date: {type: GraphQLString,}},
            async resolve(parent, args) {
                const data = await washRequests.getAllSessions(args.date)
                console.log({data})
                return data;
            },
        },

    }
});

const Mutation = new GraphQLObjectType({
    name: "Mutation",
    fields: {
        updateUser: {
            type: StudentType,
            args: {
                student_id: {type: GraphQLID},
                user_id: {type: GraphQLID},
                firstName: {type: GraphQLString},
                lastName: {type: GraphQLString},
                prevPassword: {type: GraphQLString},
                newPassword: {type: GraphQLString},
                email: {type: GraphQLString},
                phone: {type: GraphQLString},
            },
            async resolve(parent, {student_id, user_id, firstName, lastName, prevPassword, newPassword, email, phone}) {
                const student = await userRequests.updateStudent({
                    student_id,
                    user_id,
                    name: firstName,
                    surname: lastName,
                    prevPassword,
                    newPassword,
                    email,
                    phone
                });

                student.room_in_hostel = await hostelRequests.getRoomInHostel(student.room_in_hostel_id)

                return student
            },
        },
    },
});

module.exports = new GraphQLSchema({
    query: Query,
    mutation: Mutation
});