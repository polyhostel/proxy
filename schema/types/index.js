module.exports = {
    ...require('./types.hostel.graphql'),
    ...require('./types.user.graphql'),
    ...require('./types.wash.graphql')
}