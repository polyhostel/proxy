const graphql = require('graphql')
const {RoomInHostelType} = require('./types.hostel.graphql')
const {GraphQLObjectType, GraphQLString, GraphQLID, GraphQLInt} = graphql;

const userFields = {
    user_id: {type: GraphQLID},
    login: {type: GraphQLString},
    password: {type: GraphQLString},
}

const UserType = new GraphQLObjectType({
    name: 'User',
    fields: () => (userFields),
});

const studentFields = {
    user_id: {type: GraphQLID},
    student_id: {type: GraphQLInt},
    room_in_hostel: {type: RoomInHostelType},
    name: {type: GraphQLString},
    surname: {type: GraphQLString},
    patronymic: {type: GraphQLString},
    phone: {type: GraphQLString},
    email: {type: GraphQLString},
}

const StudentType = new GraphQLObjectType({
    name: 'Student',
    fields: () => (studentFields),
});

module.exports = {
    UserType,
    StudentType,
}