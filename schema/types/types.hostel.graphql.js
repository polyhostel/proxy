const graphql = require('graphql')
const {GraphQLObjectType, GraphQLString, GraphQLID} = graphql;

const roomFields = {
    room_id: {type: GraphQLID},
    name: {type: GraphQLString},
}

const RoomType = new GraphQLObjectType({
    name: 'Room',
    fields: () => (roomFields),
});

const hostelFields = {
    hostel_id: {type: GraphQLID},
    name: {type: GraphQLString},
    type: {type: GraphQLString},
}

const HostelType = new GraphQLObjectType({
    name: 'Hostel',
    fields: () => (hostelFields),
});

const roomInHostelFields = {
    room_in_hostel_id: {type: GraphQLID},
    room: {type: RoomType},
    hostel: {type: HostelType},
}

const RoomInHostelType = new GraphQLObjectType({
    name: 'RoomInHostel',
    fields: () => (roomInHostelFields),
});

module.exports = {
    HostelType,
    RoomType,
    RoomInHostelType
}