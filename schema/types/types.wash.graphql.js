const graphql = require('graphql')
const {StudentType} = require('./types.user.graphql')
const {GraphQLObjectType, GraphQLString, GraphQLID} = graphql;

const washSessionsField = {
    wash_rec_id: {type: GraphQLID},
    wash_time_id: {type: GraphQLID},
    washer_id: {type: GraphQLID},
    start_time: {type: GraphQLString},
    end_time: {type: GraphQLString},
    washer_name: {type: GraphQLString},
    date: {type: GraphQLString},
    student: {type: StudentType},
}

const WashSessionType = new GraphQLObjectType({
    name: 'WashSession',
    fields: () => (washSessionsField),
});

module.exports = {
    WashSessionType,
}