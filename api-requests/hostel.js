const axios = require('axios');

const hostelRequests = {
    getRoomInHostel: async (id) => (await axios.get(`http://hostel:3006/roominhostel?id=${id}`)).data,
}

module.exports = {
    hostelRequests
}