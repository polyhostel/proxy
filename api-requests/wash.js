const axios = require('axios');

const washRequests = {
    getAllSessions: async (date = null) => (await axios.get(`http://washservice:3009/allrecords?date=${date || ''}`)).data

}


module.exports = {
    washRequests
}