const axios = require('axios');

const userRequests = {
    getAllUsers: async () => (await axios.get('http://user:3008/allusers')).data,
    getUser: async (user_id) => (await axios.get(`http://user:3008/user?userid=${user_id}`)).data,

    getUserByLP: async (login, password) => (await axios.post(
        'http://user:3008/userbylp',
        {login, password},)).data,

    getAllStudents: async () => (await axios.get('http://user:3008/allstudents')).data,

    getStudentsByUser: async (user_id) => (await axios.get(
        `http://user:3008/studentbyuser?userid=${user_id}`)).data,

    getStudentsByRoomInHostelId: async (room_in_hostel_id) => (await axios.get(
        `http://user:3008/studentsbyroominhostel?id=${room_in_hostel_id}`)).data,

    updateStudent: async ({student_id, user_id, name, surname, prevPassword, newPassword, email, phone}) => {
        try {
            const newUser = (await axios.patch('http://user:3008/user', {
                id: user_id,
                updata: {
                    prevPassword: prevPassword,
                    password: newPassword
                }
            })).data

            if (newUser) {
                return (await axios.patch('http://user:3008/student', {
                    id: student_id,
                    updata: {
                        user_id,
                        name,
                        surname,
                        phone,
                        email
                    }
                })).data
            }

        } catch (e) {
            return e
        }
    }
}


module.exports = {
    userRequests
}