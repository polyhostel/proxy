const {userRequests} = require('./user')
const {hostelRequests} = require('./hostel')
const {washRequests} = require('./wash')

module.exports = {
    userRequests,
    hostelRequests,
    washRequests
}